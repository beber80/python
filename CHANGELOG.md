# [3.0.0](https://gitlab.com/to-be-continuous/python/compare/2.2.0...3.0.0) (2021-11-20)


### Features

* fully integration of poetry ([f0406de](https://gitlab.com/to-be-continuous/python/commit/f0406debc207728ee5ea6c71e42357ad965f7c6a))


### refacto

* **py-doc:** remove Python doc build ([10a8150](https://gitlab.com/to-be-continuous/python/commit/10a8150e1d9d43f42458846e13aec19db68cccd8))


### BREAKING CHANGES

* **py-doc:** doc job removed
this job has to been rewritten :
	- it is in a wrong stage
	- needs an other tool (make)
	- generated doc is not publish anywhere
	- no ability to choise doc generation tool

in to-be-continuous, there is mkdocs template which is able to generate python doc too

# Conflicts:
#	templates/gitlab-ci-python.yml

# [2.2.0](https://gitlab.com/to-be-continuous/python/compare/2.1.1...2.2.0) (2021-11-15)


### Features

* move packaging to a separate stage ([945fc8a](https://gitlab.com/to-be-continuous/python/commit/945fc8a8c9d298640de2860b15c7162b1ad33684))

## [2.1.1](https://gitlab.com/to-be-continuous/python/compare/2.1.0...2.1.1) (2021-11-09)


### Bug Fixes

* Use PIP_OPTS for setup.py based install ([3ea29e6](https://gitlab.com/to-be-continuous/python/commit/3ea29e634c1e1a20a1f6f84f56ed8881e570d7c2))

## [2.0.3](https://gitlab.com/to-be-continuous/python/compare/2.0.2...2.0.3) (2021-10-12)


### Bug Fixes

* disable poetry usage (py-doc) ([73d5f2a](https://gitlab.com/to-be-continuous/python/commit/73d5f2a024f72c0b28b4ef10895bd8113ff7f932))

## [2.0.2](https://gitlab.com/to-be-continuous/python/compare/2.0.1...2.0.2) (2021-10-07)


### Bug Fixes

* use master or main for production env ([77af297](https://gitlab.com/to-be-continuous/python/commit/77af297de4d99257ee286d07b1d2837948887ac5))

## [2.0.1](https://gitlab.com/to-be-continuous/python/compare/2.0.0...2.0.1) (2021-10-04)


### Bug Fixes

* disable poetry usage ([17d57cb](https://gitlab.com/to-be-continuous/python/commit/17d57cb9626de30be281c147486745df78f78545))

## [2.0.0](https://gitlab.com/to-be-continuous/python/compare/1.3.0...2.0.0) (2021-09-08)

### Features

* Change boolean variable behaviour ([4bb11b9](https://gitlab.com/to-be-continuous/python/commit/4bb11b9e2f971ba75fe1c4a36b9c1d475a6f1cb6))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.3.0](https://gitlab.com/to-be-continuous/python/compare/1.2.3...1.3.0) (2021-09-03)

### Features

* add Poetry extras support (PYTHON_POETRY_EXTRAS variable) ([e079e30](https://gitlab.com/to-be-continuous/python/commit/e079e305ddaf508d3105394df3fab1be92d6e38c))

## [1.2.3](https://gitlab.com/to-be-continuous/python/compare/1.2.2...1.2.3) (2021-07-26)

### Bug Fixes

* **poetry:** add option to disable poetry ([dbfe6f6](https://gitlab.com/to-be-continuous/python/commit/dbfe6f6b9abee4bf4aa68b603d015283a5e0bcc1))

## [1.2.2](https://gitlab.com/to-be-continuous/python/compare/1.2.1...1.2.2) (2021-06-24)

### Bug Fixes

* permission on reports directory ([f44e03a](https://gitlab.com/to-be-continuous/python/commit/f44e03a3afbad8c68babf51bc883da52bdf1c5b7))

## [1.2.1](https://gitlab.com/to-be-continuous/python/compare/1.2.0...1.2.1) (2021-06-23)

### Bug Fixes

* \"Missing git package for py-release job\" ([082f308](https://gitlab.com/to-be-continuous/python/commit/082f308330eb16a42704370190c66ba5a7823671))

## [1.2.0](https://gitlab.com/to-be-continuous/python/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([688d6f2](https://gitlab.com/to-be-continuous/python/commit/688d6f26374c4bc0610a0f979ed836c5e46c7754))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/python/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([73dbac6](https://gitlab.com/Orange-OpenSource/tbc/python/commit/73dbac6b81dcbe22b3fcfdbd34493b43fe8464a2))

## 1.0.0 (2021-05-06)

### Features

* initial release ([a1a8677](https://gitlab.com/Orange-OpenSource/tbc/python/commit/a1a867713c55fdc0ac17c3e7bd5540a7aee314cd))
